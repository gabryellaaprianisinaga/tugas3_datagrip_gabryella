use sdm;

create table employee(
    id varchar(20) not null ,
    nama varchar(50) not null ,
    atasan_id integer ,
    company_id integer
);

create table company(
    id varchar(20) not null ,
    nama varchar(50) not null ,
    alamat varchar(50) not null
);

insert into employee values
                            ('1', 'Pak Budi', null, 1),
                            ('2', 'Pak Tono', 1, 1),
                            ('3', 'Pak Totok', 1, 1),
                            ('4', 'Bu Sinta', 2, 1),
                            ('5', 'Bu Novi', 3, 1),
                            ('6', 'Andre', 4, 1),
                            ('7', 'Dono', 4, 1),
                            ('8', 'Ismir', 5, 1),
                            ('9', 'Anto', 5, 1);

insert into company values
                           ('1', 'PT JAVAN', 'Sleman'),
                           ('1', 'PT Dicoding', 'Bandung');

select * from employee;
select * from company;

# 1. Query untuk mencari CEO
select nama from employee where atasan_id is null;

# 2. Query untuk mencari siapa staff biasa
select nama from employee where atasan_id > 2;

# 3. Query untuk mencari siapa direktur
select nama from employee where atasan_id = 1;

# 4. Query untuk mencari siapa manager
select nama from employee where atasan_id = 2;

# Buat sebuah query untuk mencari jumlah bawahan dengan parameter nama.
# Pak Budi
Select COUNT(DISTINCT(child.id)) + COUNT(DISTINCT (subchild.id)) + COUNT(DISTINCT(subsubchild.id)) + COUNT(DISTINCT(subsubsubchild.id)) AS jlh_bawahan
 FROM employee AS child
 LEFT JOIN employee AS subchild ON subchild.atasan_id = child.id
 LEFT JOIN employee AS subsubchild ON subsubchild.atasan_id = subchild.id
 LEFT JOIN employee AS subsubsubchild ON subsubsubchild.atasan_id = subsubchild.id
 WHERE child.atasan_id = (SELECT id FROM employee child WHERE nama = 'Pak Budi');

# Buat sebuah query untuk mencari jumlah bawahan dengan parameter nama.
# Bu sinta
Select COUNT(DISTINCT(child.id)) + COUNT(DISTINCT (subchild.id)) + COUNT(DISTINCT(subsubchild.id)) + COUNT(DISTINCT(subsubsubchild.id)) AS jlh_bawahan
 FROM employee AS child
 LEFT JOIN employee AS subchild ON subchild.atasan_id = child.id
 LEFT JOIN employee AS subsubchild ON subsubchild.atasan_id = subchild.id
 LEFT JOIN employee AS subsubsubchild ON subsubsubchild.atasan_id = subsubchild.id
 WHERE child.atasan_id = (SELECT id FROM employee child WHERE nama = 'Bu Sinta' )


