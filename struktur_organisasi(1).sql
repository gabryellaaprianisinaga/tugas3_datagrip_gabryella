use sdm;

create table employee(
    id varchar(20) not null ,
    nama varchar(50) not null ,
    atasan_id integer ,
    jabatan varchar(50) not null
);

insert into employee values
                            ('1', 'Rizki Saputra', null, 'Direktur'),
                            ('2', 'Farhan Reza', 1, 'Manajer'),
                            ('3', 'Riyando Adi', 1, 'Manajer'),
                            ('4','Diego Manuel', 2,'Pengembangan Bisnis' ),
                            ('5', 'Satya Laksana', 2, 'Pengembangan Bisnis'),
                            ('6', 'Miguel Hernandez', 2, 'Pengembangan Bisnis'),
                            ('7', 'Putri Persada', 2, 'Pengembangan Bisnis'),
                            ('8', 'Alma Safira', 2, 'Teknisi'),
                            ('9', 'Haqi Hafiz', 2, 'Teknisi'),
                            ('10', 'Abi Isyawara', 2, 'Teknisi'),
                            ('11', 'Maman Kresna', 2, 'Teknisi'),
                            ('12', 'Nadia Aulia', 2, 'Analis'),
                            ('13', 'Mutiara Rezki', 2, 'Analis'),
                            ('14', 'Dani Setiawan', 2, 'Analis'),
                            ('15', 'Budi Putra', 2, 'Analis');

Select * from employee;

# kode SQL untuk mendapatkan semua nama pegawai dibawah manager
select * from employee
where atasan_id > 1;

# Atau
select nama from karyawan
where nama not in ('Rizki Saputra','Farhan Reza', 'Riyando Adi')

